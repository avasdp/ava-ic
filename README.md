# README #

## Purpose of this repository ##
This repository is for the development and sharing of rudimentary code for the robot. Testing individual concepts
in their own environment (e.g. basic motor control, sensory work, etc.). The code can then be adapted into the main project.

