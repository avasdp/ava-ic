#include "SDPArduino.h"
#include <Wire.h>

void setup() {
  Serial.begin(115200);
  SDPsetup();
  serial_test();
}

void serial_test() {
  helloWorld();
  Serial.println("bot: Hi! Type \"start\" to begin.");
  while (!Serial.find("start"));
  Serial.println("bot: Signal received, awaiting command. (go, stop) ");
}

void loop() {
  if ( Serial.find("go") ){
    while ( !Serial.find("stop") ){
      Serial.println("bot: moving forward");
      
      delay(2500);
      motorAllStop();
      delay(1000);
      motorForward(0,50);
      motorForward(1,50);
      delay(2500);

      Serial.println("bot: moving backward");
      delay(2500);
      motorAllStop();
      delay(1000);
      motorBackward(0,50);
      motorBackward(1,50);
      delay(2500);
      
    }
    motorAllStop();
    Serial.println("bot: STOP STOP STOP");  }

}
